﻿#include <iostream>
#include "list.h"
using namespace std;

List::~List() 
{
	for(Node *p; !isEmpty(); ) 
	{
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if(tail==0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node *tmp = new Node(el);
	if(head==0)
	{
		head = tail = tmp;
	}
	else
	{
	tail->next = tmp;
	tail = tmp;
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	char el = tail->data;
	Node *tmp = head;
	if (tail == head) 
	{
		tail = head = 0;
	}
	else 
	{
		while (tmp->next->next != 0)
		{
			tmp = tmp->next;
		}
		delete tail;
		tail = tmp;
		tmp->next = NULL;
	}
	return el;
}
bool List::search(char el) // TO DO! (Function to return True or False depending if a character is in the list.
{
	Node *tmp = head;
	for (tmp = head; tmp != tail; tmp = tmp->next)
	{
		if (el == tmp->data){
			return true;
		}
	}
	return false;
}
void List::reverse() // TO DO! (Function is to reverse the order of elements in the list.
{
	Node* tmp1 = head;
	Node* tmp2;
	Node* tmp3 = NULL;
	Node* tmp4 = head;

	while (tmp1 != NULL)
	{
		tmp2 = tmp1->next;
		tmp1->next = tmp3;
		tmp3 = tmp1;
		tmp1 = tmp2;
	}
	head = tmp3;
	tail = tmp4;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}