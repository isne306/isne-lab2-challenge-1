﻿#include <iostream>
#include "list.h"
using namespace std;

int main()
{
	//Sample Code
	List mylist;


	mylist.pushToHead('t');
	mylist.pushToHead('r');
	mylist.pushToHead('a');
	mylist.print();
	cout << endl;

	mylist.pushToTail('k');
	mylist.pushToTail('a');
	mylist.pushToTail('n');
	mylist.pushToTail('t');
	mylist.pushToTail('a');
	mylist.pushToTail('p');
	mylist.pushToTail('i');
	mylist.pushToTail('t');
	mylist.pushToTail('h');
	mylist.print();
	cout << endl;
	cout << endl;


	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list,

	  
	//- then reverse the list to see if it is a palindrome!

	cout<< "Pop from head: " << mylist.popHead() << endl;
	mylist.print();
	cout << endl;
	cout << endl;


	cout<< "Pop from tail: " << mylist.popTail() << endl;
	mylist.print();
	cout << endl;
	cout << endl;
	cout << endl;

	cout<< "Search 'r' 'a' 'z' in List: " <<endl;
	if(mylist.search('r'))
	{
     	cout << "Found r"<< endl;
	}
	if(mylist.search('a'))
	{
     	cout << "Found t" << endl;
	}
	if(mylist.search('z'))
	{
     	cout << "Found z" << endl;
	}
	else
	{
		cout << "Not found z"<< endl;
	}

	cout << endl;
	cout << endl;
	cout << "Reverse List: "<< endl;
	mylist.reverse();
	mylist.print();
	cout << endl;
	cout << endl;
}
